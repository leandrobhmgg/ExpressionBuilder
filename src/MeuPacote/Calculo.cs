﻿using System;

namespace MeuPacote
{
    public static class Calculo
    {
        public static int SomaValor(int a, int b)
        {
            return a + b;
        }

        public static int SubtraiValor(int a, int b)
        {
            return a - b;
        }

        public static int MultiplicaValor(int a, int b)
        {
            return a * b;
        }

        public static int DivisaoValor(int a, int b)
        {
            return a / b;
        }

        public static string ConcatenaValor(int a, int b)
        {
            return $"{a}{b}";
        }

        public const bool RetornaVerdadeiro = true;


        public const bool RetornaFalso = false;

        public const string RetornaA = "A";

        public const string RetornaB = "B";

        //TODO  debito tecnico 
        public static string retornaC()
        {

            return "C";
        }


    }
}