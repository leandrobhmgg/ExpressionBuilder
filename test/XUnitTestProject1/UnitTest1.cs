using MeuPacote;
using System;
using Xunit;

namespace XUnitTestProject1
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var soma = Calculo.SomaValor(2, 2);
            Assert.Equal(4, soma);
        }

        [Fact]
        public void Test2()
        {
            var soma = Calculo.SubtraiValor(3, 2);
            Assert.Equal(1, soma);
        }

        [Fact]
        public void Test3()
        {
            var soma = Calculo.MultiplicaValor(3, 2);
            Assert.Equal(6, soma);
        }
        [Fact]
        public void Test4()
        {
            var soma = Calculo.DivisaoValor(4, 2);
            Assert.Equal(2, soma);
        }

        [Fact]
        public void Test5()
        {
            var valor = Calculo.RetornaVerdadeiro;
            Assert.Equal(valor, valor);
        }

        [Fact]
        public void Test6()
        {
            var valor = Calculo.RetornaFalso;
            Assert.Equal(valor, valor);
        }
        
        [Fact]
        public void Test7()
        {
            var valor = Calculo.RetornaA;
            Assert.Equal("A", valor);
        }
        
        [Fact]
        public void Test8()
        {
            var valor = Calculo.RetornaB;
            Assert.Equal("B", valor);
        }
        [Fact]
        public void Test9()
        {
            var valor = Calculo.retornaC();
            Assert.Equal("C", valor);
        }
        //[Fact]
        //public void Test10()
        //{
        //    var valor = Calculo.ConcatenaValor(1,2);
        //    Assert.Equal("12", valor);
        //}
    }
}
