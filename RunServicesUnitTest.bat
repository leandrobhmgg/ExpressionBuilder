del c:\temp\*IntegrationTest.xml /F /Q
del c:\temp\*UnitTest.xml /F /Q
dotnet test .\test\XUnitTestProject1\\XUnitTestProject1.csproj /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:Exclude=[xunit.*]* /p:CoverletOutput="c:\temp\coverage.opencover.Services.UnitTest.xml"
IF EXIST %userprofile%\.nuget\packages\reportgenerator\4.1.2\tools\netcoreapp2.1\ReportGenerator.dll (
	dotnet %userprofile%\.nuget\packages\reportgenerator\4.1.2\tools\netcoreapp2.1\ReportGenerator.dll "-reports:c:\temp\coverage.opencover.Services.UnitTest.xml" "-targetdir:c:\temp\CoverageReportServicesUnitTest"
) ELSE (
	dotnet %NUGET_PACKAGES%\reportgenerator\4.1.2\tools\netcoreapp2.1\ReportGenerator.dll "-reports:c:\temp\coverage.opencover.Services.UnitTest.xml" "-targetdir:c:\temp\CoverageReportServicesUnitTest"
)
start c:\temp\CoverageReportServicesUnitTest\index.htm
