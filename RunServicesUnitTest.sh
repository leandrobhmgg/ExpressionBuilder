#!/bin/bash

sudo rm -rf ./test/Softplan.Pessoas.Services.UnitTest/BuildReportsServices
sudo dotnet test ./test/XUnitTestProject1/XUnitTestProject1.csproj /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:Exclude=[xunit.*]* /p:CoverletOutput=./BuildReportsServices/CoverageReportServicesUnitTest/coverage.opencover.ServicesUnitTest.xml
sudo dotnet ~/.nuget/packages/reportgenerator/4.1.2/tools/netcoreapp2.1/ReportGenerator.dll -reports:./test/XUnitTestProject1/BuildReportsServices/CoverageReportServicesUnitTest/coverage.opencover.ServicesUnitTest.xml -targetdir:./test/XUnitTestProject1/BuildReportsServices/CoverageReportServicesUnitTest
google-chrome ./test/XUnitTestProject1/BuildReportsServices/CoverageReportServicesUnitTest/index.htm
